# Java Exercise

This application is made in such a way that it will be converted into production application with just replacing the im memory repository implementation to database repository implementation.

This application is design by using TDD approach. This approach is selected to measure the correctness and completeness of real possible scenarios in automated way rather then repeating test manually again and again, which require more efforts then actually writing test.

This app has got 2 type of tests, Unit and Acceptance tests. Unit covers class level logic however acceptance test, AcceptanceTest.java, covers the test main scenarios of test.

To get more understanding on covered scenarios please refer to unit tests.
