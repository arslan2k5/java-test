package com.java.test.discount;

import com.java.test.billing.Basket;
import com.java.test.product.ProductRepository;

import java.math.BigDecimal;

public class PromotionService {
    private final DiscountRepository discountRepository;
    private final ProductRepository productRepository;

    public PromotionService(final DiscountRepository discountRepository, final ProductRepository productRepository) {
        this.discountRepository = discountRepository;
        this.productRepository = productRepository;
    }

    public BigDecimal apply(Basket basket) {
        return discountRepository.getAllDiscountOffers().stream()
                .map(discountOffer -> calculateDiscount(basket, discountOffer))
                .reduce(BigDecimal.ZERO, BigDecimal::add);

    }

    private BigDecimal calculateDiscount(Basket basket, DiscountOffer discountOffer) {

        return discountOffer.apply(basket, productRepository.getAllProducts());
    }
}
