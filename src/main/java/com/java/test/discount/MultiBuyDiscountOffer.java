package com.java.test.discount;

import com.java.test.billing.Basket;
import com.java.test.product.Product;

import java.math.BigDecimal;
import java.util.Map;

public class MultiBuyDiscountOffer implements DiscountOffer {
    private final String productName;
    private final int buyQuantity;
    private final int discountQuantity;


    public MultiBuyDiscountOffer(final String productName, final int buyQuantity, final int discountQuantity) {
        this.productName = productName;
        this.buyQuantity = buyQuantity;
        this.discountQuantity = discountQuantity;
    }

    @Override
    public BigDecimal apply(final Basket basket, final Map<String, Product> products) {
        if(products.isEmpty()) {
            return BigDecimal.ZERO;
        }

        return calculateDiscount(basket, products);
    }

    private BigDecimal calculateDiscount(final Basket basket, final Map<String, Product> products) {

        final int quantityOfPromotionProduct = basket.getItem(productName);
        final Product discountProduct = products.get(productName);
        final int totalDiscountNumbers = BigDecimal.valueOf(quantityOfPromotionProduct / (buyQuantity + discountQuantity)).toBigInteger().intValue();
        return BigDecimal.valueOf(totalDiscountNumbers).multiply(discountProduct.getCost());
    }
}
