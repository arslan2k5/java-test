package com.java.test.discount;

import com.java.test.billing.Basket;
import com.java.test.product.Product;

import java.math.BigDecimal;
import java.util.Map;

public interface DiscountOffer {

    BigDecimal apply(final Basket basket, final Map<String, Product> products);
}
