package com.java.test.billing;

import com.java.test.discount.PromotionService;
import com.java.test.product.Product;
import com.java.test.product.ProductRepository;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class StoreCheckoutService implements CheckoutService {
    private final ProductRepository productRepository;
    private final PromotionService promotionService;

    public StoreCheckoutService(final ProductRepository productRepository, final PromotionService promotionService) {
        this.productRepository = productRepository;
        this.promotionService = promotionService;
    }

    @Override
    public BigDecimal checkout(final Basket basket) {
        final BigDecimal totalAmount = basket.getItems().entrySet().stream().map((item) -> {
            final Product product = productRepository.getProductByName(item.getKey());
            return BigDecimal.valueOf(item.getValue()).multiply(product.getCost());
        }).reduce(BigDecimal.ZERO, BigDecimal::add);

        final BigDecimal totalDiscount = promotionService.apply(basket);
        return totalAmount.subtract(totalDiscount).setScale(2, RoundingMode.HALF_UP);
    }
}
