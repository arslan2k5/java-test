package com.java.test.billing;

import java.math.BigDecimal;

public interface CheckoutService {
    BigDecimal checkout(Basket basket);
}
