package com.java.test.billing;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class Basket {
    private final Map<String, Integer> items;

    public Basket() {
        this.items = new HashMap<>();
    }

    public void add(String... products) {
        Arrays.asList(products).forEach(item -> {
            items.merge(item, 1, Integer::sum);
        });
    }

    public Map<String, Integer> getItems() {
        return items;
    }

    public int getItem(final String name) {
        final Integer quantity = items.get(name);
        return Optional.ofNullable(quantity).orElse(0);
    }
}
