package com.java.test.product;

import java.math.BigDecimal;

public class Product {
    private final String name;
    private final BigDecimal cost;

    public Product(String name, BigDecimal cost) {
        this.name = name;
        this.cost = cost;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getCost() {
        return cost;
    }
}
