package com.java.test.product;

import java.util.Map;

public interface ProductRepository {
    boolean save(final Product... product);
    Product getProductByName(final String name);
    Map<String, Product> getAllProducts();
}
