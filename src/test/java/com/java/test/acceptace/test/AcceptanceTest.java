package com.java.test.acceptace.test;

import com.java.test.billing.Basket;
import com.java.test.billing.CheckoutService;
import com.java.test.billing.StoreCheckoutService;
import com.java.test.discount.*;
import com.java.test.product.InMemoryProductRepository;
import com.java.test.product.Product;
import com.java.test.product.ProductRepository;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class AcceptanceTest {


    @Test
    public void calculateBasketPrice() {
        //GIVEN
        Basket basket = new Basket();
        basket.add(APPLE, BANANA);

        //WHEN
        final BigDecimal amount = checkoutService.checkout(basket);

        //THEN
        assertThat(amount, is(new BigDecimal("0.55")));
    }
    @Test
    public void shouldApplyDiscountOnMelons() {
        //GIVEN
        Basket basket = new Basket();
        basket.add(MELON, MELON);

        //WHEN
        final BigDecimal amount = checkoutService.checkout(basket);

        //THEN
        assertThat(amount, is(new BigDecimal("0.50")));
    }

    @Test
    public void shouldApplyDiscountOnLimes() {
        //GIVEN
        Basket basket = new Basket();
        basket.add(LIME, LIME, LIME);

        //WHEN
        final BigDecimal amount = checkoutService.checkout(basket);

        //THEN
        assertThat(amount, is(new BigDecimal("0.30")));
    }

    @Test
    public void shouldNotDiscountIfQuantityDoesNotMatchWithOffer() {
        //GIVEN
        Basket basket = new Basket();
        basket.add(LIME, LIME, MELON);

        //WHEN
        final BigDecimal amount = checkoutService.checkout(basket);

        //THEN
        assertThat(amount, is(new BigDecimal("0.80")));
    }

    @Test
    public void shouldApplyDiscountOnAllItemsBasket() {
        //GIVEN
        Basket basket = new Basket();
        basket.add(LIME, LIME, LIME, MELON, MELON, BANANA, APPLE);

        //WHEN
        final BigDecimal amount = checkoutService.checkout(basket);

        //THEN
        assertThat(amount, is(new BigDecimal("1.35")));
    }

    private void initDiscounts() {
        final DiscountOffer melonDiscount = new MultiBuyDiscountOffer(MELON, 1, 1);
        final DiscountOffer limeDiscount = new MultiBuyDiscountOffer(LIME, 2, 1);
        discountRepository.add(melonDiscount, limeDiscount);
    }

    private void initProducts() {
        final Product apple = new Product(APPLE,new BigDecimal("0.35"));
        final Product banana = new Product(BANANA,new BigDecimal("0.20"));
        final Product melon = new Product(MELON,new BigDecimal("0.50"));
        final Product lime = new Product(LIME,new BigDecimal("0.15"));
        productRepository.save(apple, banana, melon, lime);
    }

    @Before
    public void setup(){
        initProducts();
        initDiscounts();
    }


    private final DiscountRepository discountRepository = new InMemoryDiscountRepository();
    private final ProductRepository productRepository = new InMemoryProductRepository();
    private final PromotionService promotionService = new PromotionService(discountRepository, productRepository);
    private final CheckoutService checkoutService = new StoreCheckoutService(productRepository, promotionService);

    private static final String APPLE = "apple";
    private static final String BANANA = "banana";
    private static final String MELON = "melon";
    private static final String LIME = "lime";
}
