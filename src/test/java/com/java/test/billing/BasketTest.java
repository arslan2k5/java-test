package com.java.test.billing;

import org.junit.Test;

import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class BasketTest {
    private final Basket basket = new Basket();

    @Test
    public void addAProduct() {
        //WHEN
        basket.add("Apple");

        //THEN
        assertThat(basket.getItem("Apple"), is(1));
    }

    @Test
    public void addMultipleProduct() {
        //WHEN
        basket.add("Apple", "Banana");

        //THEN
        assertThat(basket.getItem("Apple"), is(1));
        assertThat(basket.getItem("Banana"), is(1));
    }

    @Test
    public void getAllItems() {
        //GIVEN
        basket.add("Apple", "Banana");

        //WHEN
        final Map<String, Integer> items = basket.getItems();

        //THEN
        assertThat(items.size(), is(2));
        assertThat(items.get("Apple"), is(1));
        assertThat(items.get("Banana"), is(1));
    }

    @Test
    public void addSameProductMultipleTimes() {
        //WHEN
        basket.add("Apple", "Apple");

        //THEN
        assertThat(basket.getItem("Apple"), is(2));
    }

    @Test
    public void returnZeroWhenThereIsNoItem() {
        //WHEN
        //NO items added
        //THEN
        assertThat(basket.getItem("Apple"), is(0));
    }
}