package com.java.test.billing;

import com.google.common.collect.ImmutableMap;
import com.java.test.discount.PromotionService;
import com.java.test.product.Product;
import com.java.test.product.ProductRepository;
import org.junit.Test;

import java.math.BigDecimal;

import static java.util.Collections.emptyMap;
import static java.util.Collections.singletonList;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

public class StoreCheckoutServiceTest {

    private static final String PRODUCT_NAME = "productName";
    private final ProductRepository productRepository = mock(ProductRepository.class);
    private final PromotionService promotionService = mock(PromotionService.class);

    private final StoreCheckoutService checkoutService = new StoreCheckoutService(productRepository, promotionService);


    @Test
    public void shouldSuccessfullyGenerateBill() {
        //GIVEN
        final Basket basket = mock(Basket.class);
        when(basket.getItems()).thenReturn(ImmutableMap.of(PRODUCT_NAME, 1));
        when(productRepository.getProductByName(PRODUCT_NAME)).thenReturn(new Product(PRODUCT_NAME, BigDecimal.ONE));
        when(promotionService.apply(basket)).thenReturn(BigDecimal.ZERO);

        //WHEN
        final BigDecimal totalAmount = checkoutService.checkout(basket);

        //THEN
        verify(productRepository).getProductByName(PRODUCT_NAME);
        verify(promotionService).apply(basket);

        assertThat(totalAmount, is(new BigDecimal("1.00")));
    }

    @Test
    public void shouldSuccessfullyApplyDiscount() {
        //GIVEN
        final Basket basket = mock(Basket.class);
        when(basket.getItems()).thenReturn(ImmutableMap.of(PRODUCT_NAME, 1, "productName2", 2));
        when(productRepository.getProductByName(PRODUCT_NAME)).thenReturn(new Product(PRODUCT_NAME, BigDecimal.ONE));
        when(productRepository.getProductByName("productName2")).thenReturn(new Product(PRODUCT_NAME, BigDecimal.ONE));
        when(promotionService.apply(basket)).thenReturn(BigDecimal.ONE);

        //WHEN
        final BigDecimal totalAmount = checkoutService.checkout(basket);

        //THEN
        verify(productRepository).getProductByName(PRODUCT_NAME);
        verify(promotionService).apply(basket);

        assertThat(totalAmount, is(new BigDecimal("2.00")));
    }

    @Test
    public void shouldSuccessfullyGenerateAmountRoundTo2DecimalPlaces() {
        //GIVEN
        final Basket basket = mock(Basket.class);
        when(basket.getItems()).thenReturn(ImmutableMap.of(PRODUCT_NAME, 1));
        when(productRepository.getProductByName(PRODUCT_NAME)).thenReturn(new Product(PRODUCT_NAME, new BigDecimal("0.9123")));
        when(promotionService.apply(basket)).thenReturn(BigDecimal.ZERO);

        //WHEN
        final BigDecimal totalAmount = checkoutService.checkout(basket);

        //THEN
        verify(productRepository).getProductByName(PRODUCT_NAME);
        verify(promotionService).apply(basket);

        assertThat(totalAmount, is(new BigDecimal("0.91")));
    }

    @Test
    public void shouldSuccessfullyGenerateAmountRoundTo2DecimalPlacesUp() {
        //GIVEN
        final Basket basket = mock(Basket.class);
        when(basket.getItems()).thenReturn(ImmutableMap.of(PRODUCT_NAME, 1));
        when(productRepository.getProductByName(PRODUCT_NAME)).thenReturn(new Product(PRODUCT_NAME, new BigDecimal("1.9556")));
        when(promotionService.apply(basket)).thenReturn(BigDecimal.ZERO);

        //WHEN
        final BigDecimal totalAmount = checkoutService.checkout(basket);

        //THEN
        verify(productRepository).getProductByName(PRODUCT_NAME);
        verify(promotionService).apply(basket);

        assertThat(totalAmount, is(new BigDecimal("1.96")));
    }

    @Test
    public void shouldSuccessfullyGenerateBillWithNoAmountWhenBasketIsEmpty() {
        //GIVEN
        final Basket basket = mock(Basket.class);
        when(basket.getItems()).thenReturn(emptyMap());
        when(promotionService.apply(basket)).thenReturn(BigDecimal.ZERO);

        //WHEN
        final BigDecimal totalAmount = checkoutService.checkout(basket);

        //THEN
        verify(productRepository, times(0)).getProductByName(PRODUCT_NAME);
        verify(promotionService).apply(basket);

        assertThat(totalAmount, is(new BigDecimal("0.00")));
    }
}