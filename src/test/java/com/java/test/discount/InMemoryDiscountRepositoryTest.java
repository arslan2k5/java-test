package com.java.test.discount;

import org.junit.Test;

import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;


public class InMemoryDiscountRepositoryTest {

    private final InMemoryDiscountRepository repository = new InMemoryDiscountRepository();

    @Test
    public void canAddDiscountOffers() {
        //WHEN
        boolean isAdded = repository.add(mock(MultiBuyDiscountOffer.class));

        //THEN
        assertTrue(isAdded);
    }

    @Test
    public void canAddMultipleDiscountOffers() {
        //WHEN
        final MultiBuyDiscountOffer discountOffer1 = mock(MultiBuyDiscountOffer.class);
        final MultiBuyDiscountOffer discountOffer2 = mock(MultiBuyDiscountOffer.class);
        boolean isAdded = repository.add(discountOffer1, discountOffer2);

        //THEN
        assertTrue(isAdded);
        final List<DiscountOffer> discountOffers = repository.getAllDiscountOffers();
        assertThat(discountOffers.size(), is(2));
        assertThat(discountOffers.get(0), is(discountOffer1));
        assertThat(discountOffers.get(1), is(discountOffer2));
    }

    @Test
    public void getAllDiscounts() {
        //GIVEN
        MultiBuyDiscountOffer discountOffer = mock(MultiBuyDiscountOffer.class);
        repository.add(discountOffer);

        //WHEN
        List<DiscountOffer> allDiscountOffers = repository.getAllDiscountOffers();

        //THEN
        assertThat(allDiscountOffers.size(), is(1));
        assertThat(allDiscountOffers.get(0), is(discountOffer));
    }
}