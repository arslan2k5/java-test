package com.java.test.discount;

import com.java.test.billing.Basket;
import com.java.test.product.Product;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MultiBuyDiscountOfferTest {

    private static final String PRODUCT = "product1";
    private final DiscountOffer buy1Get1Offer = new MultiBuyDiscountOffer(PRODUCT, 1, 1 );
    private final DiscountOffer buy2Get1Offer = new MultiBuyDiscountOffer(PRODUCT, 2, 1 );


    @Test
    public void calculateDiscount() {
        //GIVEN
        final Map<String, Product> products = mock(Map.class);
        final Basket basket = mock(Basket.class);

        when(basket.getItem(PRODUCT)).thenReturn(2);
        when(products.get(PRODUCT)).thenReturn(new Product(PRODUCT, BigDecimal.valueOf(1)));

        //WHEN
        final BigDecimal discount = buy1Get1Offer.apply(basket, products);

        //THEN
        assertThat(discount, is(BigDecimal.ONE));
    }

    @Test
    public void calculateMultipleDiscounts() {
        //GIVEN
        final Map<String, Product> products = mock(Map.class);
        final Basket basket = mock(Basket.class);

        when(basket.getItem(PRODUCT)).thenReturn(4);
        when(products.get(PRODUCT)).thenReturn(new Product(PRODUCT, BigDecimal.valueOf(1)));

        //WHEN
        final BigDecimal discount = buy1Get1Offer.apply(basket, products);

        //THEN
        assertThat(discount, is(BigDecimal.valueOf(2)));
    }

    @Test
    public void applyOnly1DiscountWhen1DiscountIsApplicable() {
        //GIVEN
        final Map<String, Product> products = mock(Map.class);
        final Basket basket = mock(Basket.class);

        when(basket.getItem(PRODUCT)).thenReturn(3);
        when(products.get(PRODUCT)).thenReturn(new Product(PRODUCT, BigDecimal.ONE));

        //WHEN
        final BigDecimal discount = buy1Get1Offer.apply(basket, products);

        //THEN
        assertThat(discount, is(BigDecimal.valueOf(1)));
    }

    @Test
    public void applyDiscountWhenBuy2Get1() {
        //GIVEN
        final Map<String, Product> products = mock(Map.class);
        final Basket basket = mock(Basket.class);

        when(basket.getItem(PRODUCT)).thenReturn(3);
        when(products.get(PRODUCT)).thenReturn(new Product(PRODUCT, BigDecimal.ONE));

        //WHEN
        final BigDecimal discount = buy2Get1Offer.apply(basket, products);

        //THEN
        assertThat(discount, is(BigDecimal.valueOf(1)));
    }

    @Test
    public void doNotApplyDiscountWhenBought1OnBuy2Get1() {
        //GIVEN
        final Map<String, Product> products = mock(Map.class);
        final Basket basket = mock(Basket.class);

        when(basket.getItem(PRODUCT)).thenReturn(1);
        when(products.get(PRODUCT)).thenReturn(new Product(PRODUCT, BigDecimal.ONE));

        //WHEN
        final BigDecimal discount = buy2Get1Offer.apply(basket, products);

        //THEN
        assertThat(discount, is(BigDecimal.valueOf(0)));
    }

    @Test
    public void applyMultipleDiscountsOnBuy2Get1() {
        //GIVEN
        final Map<String, Product> products = mock(Map.class);
        final Basket basket = mock(Basket.class);

        when(basket.getItem(PRODUCT)).thenReturn(6);
        when(products.get(PRODUCT)).thenReturn(new Product(PRODUCT, BigDecimal.ONE));

        //WHEN
        final BigDecimal discount = buy2Get1Offer.apply(basket, products);

        //THEN
        assertThat(discount, is(BigDecimal.valueOf(2)));
    }

    @Test
    public void applyMultipleDiscountsOnBuy2Get1WhenBoughtManyDiscountedProducts() {
        //GIVEN
        final Map<String, Product> products = mock(Map.class);
        final Basket basket = mock(Basket.class);

        when(basket.getItem(PRODUCT)).thenReturn(5);
        when(products.get(PRODUCT)).thenReturn(new Product(PRODUCT, BigDecimal.ONE));

        //WHEN
        final BigDecimal discount = buy2Get1Offer.apply(basket, products);

        //THEN
        assertThat(discount, is(BigDecimal.valueOf(1)));
    }

    @Test
    public void dontApplyDiscountWhenBasketHasNoProductOnOffer() {
        //GIVEN
        final Map<String, Product> products = mock(Map.class);
        final Basket basket = mock(Basket.class);

        when(basket.getItem(PRODUCT)).thenReturn(0);
        when(products.get(PRODUCT)).thenReturn(new Product(PRODUCT, BigDecimal.ONE));

        //WHEN
        final BigDecimal discount = buy2Get1Offer.apply(basket, products);

        //THEN
        assertThat(discount, is(BigDecimal.valueOf(0)));
    }

    @Test
    public void dontApplyDiscountWhenProductsDoesNotExists() {
        //GIVEN
        final Map<String, Product> products = mock(Map.class);
        final Basket basket = mock(Basket.class);

        when(basket.getItem(PRODUCT)).thenReturn(0);
        when(products.isEmpty()).thenReturn(true);

        //WHEN
        final BigDecimal discount = buy2Get1Offer.apply(basket, products);

        //THEN
        assertThat(discount, is(BigDecimal.valueOf(0)));
    }
}