package com.java.test.discount;

import com.java.test.billing.Basket;
import com.java.test.product.Product;
import com.java.test.product.ProductRepository;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.List;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PromotionServiceTest {
    private static final String PRODUCT_NAME = "productName";
    private final DiscountRepository discountRepository = mock(DiscountRepository.class);
    private final ProductRepository productRepository = mock(ProductRepository.class);
    private final PromotionService promotionService = new PromotionService(discountRepository, productRepository);

    @Test
    public void shouldApplyDiscount() {
        //GIVEN
        final Basket basket = mock(Basket.class);
        final MultiBuyDiscountOffer multiBuyDiscountOffer = mock(MultiBuyDiscountOffer.class);
        final List<DiscountOffer> discountOffers = singletonList(multiBuyDiscountOffer);
        when(discountRepository.getAllDiscountOffers()).thenReturn(discountOffers);
        final Product product = new Product(PRODUCT_NAME, BigDecimal.ONE);
        when(productRepository.getProductByName(PRODUCT_NAME)).thenReturn(product);
        when(multiBuyDiscountOffer.apply(any(), any())).thenReturn(BigDecimal.ONE);
        //WHEN
        final BigDecimal totalDiscount = promotionService.apply(basket);

        //THEN
        assertThat(totalDiscount, is(BigDecimal.ONE));
    }

    @Test
    public void shouldApplyDiscountOnSameProduct() {
        //GIVENs
        final Basket basket = mock(Basket.class);
        final MultiBuyDiscountOffer multiBuyDiscountOffer = mock(MultiBuyDiscountOffer.class);
        final List<DiscountOffer> discountOffers = singletonList(multiBuyDiscountOffer);
        when(discountRepository.getAllDiscountOffers()).thenReturn(discountOffers);
        final Product product = new Product(PRODUCT_NAME, BigDecimal.ONE);
        when(productRepository.getProductByName(PRODUCT_NAME)).thenReturn(product);
        when(multiBuyDiscountOffer.apply(any(), any())).thenReturn(BigDecimal.ONE);
        //WHEN
        final BigDecimal totalDiscount = promotionService.apply(basket);

        //THEN
        assertThat(totalDiscount, is(BigDecimal.ONE));
    }

    @Test
    public void shouldNotApplyDiscountWhenThereAreNoDiscounts() {
        //GIVEN
        final Basket basket = mock(Basket.class);
        final List<DiscountOffer> discountOffers = emptyList();
        when(discountRepository.getAllDiscountOffers()).thenReturn(discountOffers);

        //WHEN
        final BigDecimal totalDiscount =  promotionService.apply(basket);

        //THEN
        assertThat(totalDiscount, is(BigDecimal.ZERO));
    }

}